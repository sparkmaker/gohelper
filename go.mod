module bitbucket.org/sparkmaker/gohelper

go 1.14

require (
	github.com/Shopify/sarama v1.26.1
	github.com/bluele/gcache v0.0.0-20190518031135-bc40bd653833
	github.com/bsm/sarama-cluster v2.1.15+incompatible
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/globalsign/mgo v0.0.0-20181015135952-eeefdecb41b8
	github.com/google/uuid v1.1.1
	github.com/onsi/ginkgo v1.12.0 // indirect
	github.com/onsi/gomega v1.9.0 // indirect
)
