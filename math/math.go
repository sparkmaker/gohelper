package math

import (
	"fmt"
	m "math"
	"strconv"
)

type Float64 struct {
	prec uint
	err  error
}

func NewFloat64(maxPrec uint) *Float64 {
	if maxPrec > 5 {
		panic("this module does not support precision more than 5")
	}
	return &Float64{prec: maxPrec}
}

func (f *Float64) Plus(x, y float64) float64 {
	return f.accurate(x + y)
}
func (f *Float64) Minus(x, y float64) float64 {
	return f.accurate(x - y)
}
func (f *Float64) Multiply(x, y float64) float64 {
	return f.accurate(x * y)
}
func (f *Float64) Divide(x, y float64) float64 {
	return f.accurate(x / y)
}
func (f *Float64) Percentage(x float64) float64 {
	return f.accurate(x / 100)
}
func (f *Float64) Error() error {
	return f.err
}

func (f *Float64) accurate(x float64) (r float64) {
	p := m.Pow10(int(f.prec))
	nx := m.Round(x*p) / p
	fm := "%." + strconv.Itoa(int(f.prec)) + "f"
	r, f.err = strconv.ParseFloat(fmt.Sprintf(fm, nx), 64)
	return
}
